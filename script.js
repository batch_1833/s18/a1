// console.log("Hello World");

let trainer ={
	name: "Ash Ketchum",
	age: 10 ,
	Pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {
		hoen: ["May" , "Max"],
		kanto: ["Brock", "Misty"]
	},
	talk: function(){
		console.log("Result of talk method")
		console.log(this.Pokemon[0]+" I choose you")
	}
	
}
let objArr = [trainer];
console.log(trainer);
console.log("Result of dot notation:");
console.log(trainer.name);
console.log("Result of square bracket notation:");
console.log(objArr[0]['Pokemon']);
trainer.talk();



 // using object literals notation
 let myPokemon= {
 	name: "Pikachu",
 	level: 50,
 	health: 100,
 	attack: 50,
 	tackle: function(){
 		console.log("This pokemon tackled targetPokemon")
 		console.log("targetPokemon's health is reduced to newTargetPokemonHealth");
	},
 	faint: function(){
 		console.log("Pokemon fainted");
 	}
 }


 // Object Contructor Notation
 function Pokemon(name, level){
 	// Properties
 	this.name = name;
 	this.level = level;
 	this.health = level * 2;
 	this.attack = level;

 	// Methods			//targetPokemon
 	this.tackle = function(target){
 					//pokemonObject		//targetPokemon
 		console.log(this.name + " tackled " + target.name);
 		// targetHealth - pokemonAttack = newHealth
 		
 		console.log(target.name+" health is reduced to -"+this.attack);
 		target.health =  target.health - this.attack;
 

 		if(target.health<=0){
 			target.faint();
 			console.log(target);

 		}
 		else{
 			console.log(target);
 		}
 		

 	}
 	this.faint = function(){
 		console.log(this.name+ " fainted.")
 	}

 };
 let pikachu = new Pokemon("Pikachu", 100);
 console.log(pikachu);

 let charizard = new Pokemon("Charizard", 20);
 console.log(charizard);

 let squirtle = new Pokemon("Squirtle", 40);
 console.log(squirtle);

 let bulbasaur = new Pokemon("Bulbasaur", 30);
 console.log(bulbasaur);

 charizard.tackle(bulbasaur);
  pikachu.tackle(squirtle);
